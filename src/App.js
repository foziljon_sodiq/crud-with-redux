import {useSelector, useDispatch} from "react-redux";
import TodosHeader from "./components/todos-header";
import TodosForm from "./components/todos-form";
import TodoItems from "./components/todos-group";
import TodosFooter from "./components/todos-footer";
import { useEffect } from "react";
import { initializeTodo } from "./action/action";


function App() {
  const todosLength = useSelector(state => state.todos)
  const dispatch = useDispatch()
  const storageTodo = JSON.parse(localStorage.getItem("todos")) 
  useEffect(()=>{
      dispatch(initializeTodo(storageTodo))
  }, [])

  return (
    <div className="container">
      <div className="row">
        <div className="col-6 offset-3">
          <div className="card mt-5">
            <TodosHeader length={todosLength.length}/> 
            <TodosForm/> 
            <TodoItems/> 
            <TodosFooter/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
