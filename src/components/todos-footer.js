import { useSelector } from "react-redux";

const TodosFooter = () => {
  const todos = useSelector((store) => store.todos);

  const handleSave = () => {
    localStorage.setItem("todos", JSON.stringify(todos));
  };

  return (
    <div className="card-footer">
      <button className="btn btn-primary" onClick={handleSave}>
        Save
      </button>
    </div>
  );
};
export default TodosFooter;
