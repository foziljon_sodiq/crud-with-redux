import { useRef } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../action/action";
import { v4 as uuidv4 } from "uuid";

const TodosForm = () => {
  const inputRef = useRef(null);
  const dispatch = useDispatch();
  const formRef = useRef(null)

  const handleSubmit = (e) => {
    e.preventDefault(); 
    dispatch(addTodo({
       title: inputRef.current.value,
       id: uuidv4(),
       idDone: false
    }))
    formRef.current.reset()
  };

  return (
    <div className="card-body">
      <form className="mb-3" onSubmit={handleSubmit} ref={formRef}>
        <label htmlFor="exampleFormControlInput1" className="form-label">
          <h5>Add new Value</h5>
        </label>
        <input
          ref={inputRef}
          type="text"
          className="form-control"
          id="exampleFormControlInput1"
          placeholder="example"
        />
        <button className="btn btn-success form-control mt-3">Add</button>
      </form>
    </div>
  );
};
export default TodosForm;
