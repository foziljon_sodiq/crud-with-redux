import TodoItem from "./todos-item";
import { useSelector } from "react-redux";

const TodoItems = () => {
  const todos = useSelector((state) => state.todos); 

  return (
    <ul className="list-group list-group-flash">
      {todos.length > 0 ? (
        todos.map((item, id) => {
          return <TodoItem item={item} key={id}  />;
        })
      ) : (
        <h1>"NOT YET"</h1>
      )}
    </ul>
  );
};
export default TodoItems;
