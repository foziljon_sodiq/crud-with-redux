import { useDispatch } from "react-redux";
import { removeTodo, markTodoDone } from "../action/action";

const TodoItem = ({ item }) => {
  const id = item.id;
  const title = item.title;
  const isDone = item.isDone
  const dispatch = useDispatch();

  const handMarkTodo = (e) => {
    dispatch(
      markTodoDone({
        id,
        title,
        isDone: e.target.checked,
      })
    );
  };

  return (
    <li className="list-group-item d-flex justify-content-between">
      <span>
        <input
          className="form-check-input position-static me-4 "
          type="checkbox"
          id="blankCheckbox"
          onChange={handMarkTodo}
        />
        <label className={ isDone ? "text-decoration-line-through" : ""}  >{title}</label>
      </span>

      <button
        className="btn btn-danger"
        onClick={() => dispatch(removeTodo(id))}
      > 
        removed 
      </button>
    </li>
  );
};
export default TodoItem;
